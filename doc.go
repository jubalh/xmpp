// Copyright 2014 Sam Whited.
// Use of this source code is governed by the BSD 2-clause license that can be
// found in the LICENSE file.

// Package xmpp provides functionality from the Extensible Messaging and
// Presence Protocol (more commonly known as XMPP).
//
// Be advised: This API is still unstable and is subject to change.
package xmpp // import "bitbucket.org/mellium/xmpp"
