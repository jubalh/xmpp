# Contributing

Bugs, feature requests, and other discussions can be started by opening an
[issue][issues].

If you want to contribute a pull request, make sure you do the following first:

  - Format all code with `go fmt`
  - Write documentation comments for your code
  - Write tests for your code
  - Follow Go [best practices][bp]


[issues]: https://bitbucket.org/mellium/mel/issues?status=new&status=open
[bp]: http://talks.golang.org/2013/bestpractices.slide
