// Copyright 2015 Sam Whited.
// Use of this source code is governed by the BSD 2-clause license that can be
// found in the LICENSE file.

// The server package is used to manage C2S, S2S, and component connections to
// your XMPP server or component. It provides a higher level API for creating
// and managing XMPP servers (without digging down in to the nitty gritty XML
// details).
//
// Be advised: This API is still unstable and is subject to change.
package server // import "bitbucket.org/mellium/xmpp/server"
