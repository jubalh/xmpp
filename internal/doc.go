// Copyright 2016 Sam Whited.
// Use of this source code is governed by the BSD 2-clause license that can be
// found in the LICENSE file.

// Package internal provides non-exported functionality used by xmpp and its
// child packages.
package internal // import "bitbucket.org/mellium/xmpp/internal"
