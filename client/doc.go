// Copyright 2016 Sam Whited.
// Use of this source code is governed by the BSD 2-clause license that can be
// found in the LICENSE file.

// The client package provides a higher level API for creating and managing XMPP
// clients without digging down into the underlying protocol.
//
// Be advised: This API is still unstable and is subject to change.
package client // import "bitbucket.org/mellium/xmpp/client"
