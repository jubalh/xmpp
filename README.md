# Koiné XMPP

[![GoDoc](https://godoc.org/bitbucket.org/mellium/xmpp?status.svg)](https://godoc.org/bitbucket.org/mellium/xmpp)
[![License](https://img.shields.io/badge/license-FreeBSD-blue.svg)](https://opensource.org/licenses/BSD-2-Clause)

**Koiné** is an XMPP library in Go originally designed for the
[Mellium][mellium] server project.

To use it in your project, import it (or one of its subproject's) like so:

```go
import bitbucket.org/mellium/xmpp
```

## Issues and feature requests

To file a bug report or submit a feature request, please use the main Mellium
[issue tracker][issues] and select the appropriate component (eg. "XMPP" or
"Server").

## License

The package may be used under the terms of the BSD 2-Clause License a copy of
which may be found in the file [LICENSE.md][LICENSE].

[mellium]: https://bitbucket.org/mellium
[issues]: https://bitbucket.org/mellium/mel/issues?status=new&status=open
[LICENSE]: ./LICENSE.md
