// Copyright 2014 Sam Whited.
// Use of this source code is governed by the BSD 2-clause license that can be
// found in the LICENSE file.

// Package jid implements XMPP addresses (historically called "Jabber ID's" or
// "JID's") as described in RFC 7622.
//
// Be advised: This API is still unstable and is subject to change.
package jid // import "bitbucket.org/mellium/xmpp/jid"
